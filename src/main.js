import Vue from 'vue'
import Vuelidate from 'vuelidate'
import App from './App.vue'
import router from './router.js'
import store from './store/store.js'
import dateFilter from "@/filters/date.filter"
import currencyFilter from "@/filters/currency.filter"
import messagePlugin from '@/utils/message.plugin'
import Loader from "@/components/app/Loader"
import './registerServiceWorker'
import 'materialize-css/dist/js/materialize.min'

import firebase from "firebase/app"
import 'firebase/auth'
import 'firebase/database'

Vue.config.productionTip = false

Vue.use(messagePlugin)
Vue.use(Vuelidate)
Vue.filter('date', dateFilter)
Vue.filter('currency', currencyFilter)
Vue.component('Loader', Loader)

const firebaseConfig = {
  apiKey: "AIzaSyBJc2MTMn6XCxVmtKF9j-qpXDuMKB8APwU",
  authDomain: "vue-lesson-22043.firebaseapp.com",
  databaseURL: "https://vue-lesson-22043.firebaseio.com",
  projectId: "vue-lesson-22043",
  storageBucket: "vue-lesson-22043.appspot.com",
  messagingSenderId: "139397337624",
  appId: "1:139397337624:web:11e758a6ecc0b395be696d"
}

firebase.initializeApp(firebaseConfig)

let app

firebase.auth().onAuthStateChanged(() => {
  if (!app) {
    app = new Vue({
      router,
      store,
      render: h => h(App)
    }).$mount('#app')
  }
})




